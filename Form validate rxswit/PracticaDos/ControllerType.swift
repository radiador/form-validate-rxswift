//
//  ControllerType.swift
//  Form validate rxswit
//
//  Created by formador on 11/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit
import RxSwift

protocol ControllerType: class {
    
    associatedtype ViewModelType: ViewModelProtocol
    
    func configure(with viewModel: ViewModelType)
    
    static func create(with viewModel: ViewModelType) -> UIViewController
}

protocol ViewModelProtocol: class {
    
    associatedtype Input
    associatedtype Output
}

class LoginControllerViewModel: ViewModelProtocol {
    
    private let disposeBag = DisposeBag()
    
    struct Input {
        
        let email: AnyObserver<String>
        let pasword: AnyObserver<String>
        let sigInDidTap: AnyObserver<Void>
    }
    
    struct Output {
        
        let loginResultObservable: Observable<User>
//        let errorObservable: Observable<Error>
    }
    
    private let emailSubject = PublishSubject<String>()
    private let passwordSubject = PublishSubject<String>()
    private let signInDidTapSubject = PublishSubject<Void>()
    private let loginResultSubject = PublishSubject<User>()
    
    let input: Input
    let ouput: Output
    
    init(loginService: LoginService) {
        
        input = Input(email: emailSubject.asObserver(), pasword: passwordSubject.asObserver(), sigInDidTap: signInDidTapSubject.asObserver())
        
        ouput = Output(loginResultObservable: loginResultSubject.asObservable())

        
        let emailValidObservable = emailSubject.asObservable()
            .filter { email -> Bool in
                return email.count > 4
        }.debug("emailValidObservable5", trimOutput: true)
        
        let passwordValidObservable = passwordSubject.asObservable()
            .filter { email -> Bool in
                return email.count > 4
        }.debug("passwordValidObservable", trimOutput: true)
        
        let credentialsObsevable = Observable.combineLatest(emailValidObservable, passwordValidObservable) { (email, password) in
            
            Credentials(email: email, password: password)
        }.debug("combineLatest", trimOutput: true)
        
        signInDidTapSubject
            .withLatestFrom(credentialsObsevable)
            .debug("withLatestFrom", trimOutput: true)
            .flatMap { credentials in
                return loginService.singIn(credentials: credentials)
            }
            .subscribe(onNext: { user in
                self.loginResultSubject.onNext(user)
            }).disposed(by: disposeBag)
        
    }
}

struct Credentials {
    
    let email: String
    let password: String
}

struct User {
    
}

class LoginService {
    
    func singIn(credentials: Credentials) -> Observable<User> {
        
        //Simulando la llamada a un servicio
        
        return Observable.just(User())
    }
}
