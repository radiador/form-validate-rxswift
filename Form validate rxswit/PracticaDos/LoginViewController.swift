//
//  LoginViewController.swift
//  
//
//  Created by formador on 11/4/19.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController, ControllerType {
    
    typealias ViewModelType  = LoginControllerViewModel
    
    var viewModel: ViewModelType!
    
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure(with: viewModel)
    }
    
    func configure(with viewModel: LoginViewController.ViewModelType) {
        
     emailTextField.rx.text.orEmpty
        .bind(to: viewModel.input.email)
        .disposed(by: disposeBag)

        passwordTextField.rx.text.orEmpty
            .bind(to: viewModel.input.pasword)
        .disposed(by: disposeBag)
        
        signInButton.rx.tap
            .bind(to: viewModel.input.sigInDidTap)
        .disposed(by: disposeBag)
        
        viewModel.ouput.loginResultObservable
            .subscribe(onNext: { user in
                print("Usuario logado")
            }, onError: { error in
                print("Error \(error)")
            })
        .disposed(by: disposeBag)
        
    }
}

extension LoginViewController {
    
    static func create(with viewModel: LoginViewController.ViewModelType) -> UIViewController {
        
        let storyboard = UIStoryboard(name: "Second", bundle: nil)
        
        if let viewController = storyboard.instantiateViewController(withIdentifier: "loginViewControllerIdentifier") as? LoginViewController {
            
            viewController.viewModel = viewModel
            
            return viewController
        }
        
        return UIViewController()
    }
}


