//
//  ValidationViewController.swift
//  Form validate rxswit
//
//  Created by formador on 11/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ValidationViewController: UIViewController {
    
    @IBOutlet weak var userNameValidationLabel: UILabel!
    @IBOutlet weak var userNameRemainderLabel: UILabel!
    @IBOutlet weak var passwordInvalidLabel: UILabel!
    
    @IBOutlet weak var userNameTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loginButton.isEnabled = false
        
        configureReaction()
    }
    
    let disposeBag = DisposeBag()
    
    private func configureReaction() {
        
        let userMinCharacters = 5
        
        //Activación boton login
        let userNameObservable = userNameTxt.rx.text.orEmpty.skip(1).debug("userNameTxt", trimOutput: true)
        
        let userNameValidObservable = userNameObservable
            .map { text -> Bool in
                return text.count > userMinCharacters
            }
        .debug("userNameValidObservable", trimOutput: true)

        let passwordValidObservable = passwordTxt.rx.text.orEmpty.skip(1)
            .map { text -> Bool in
                return text.count > 8
            }
            .debug("passwordValidObservable", trimOutput: true)

        
        Observable.combineLatest(userNameValidObservable, passwordValidObservable) { userValid, passValid in
            return userValid && passValid
            }
            .debug("combineLatest", trimOutput: true)
            .bind(to: loginButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        
        //Informacion usuario sobre validez del nombre
        userNameObservable
            .map { text -> String in
                let currentLenght = text.count
                return "\(currentLenght) de \(userMinCharacters)"
        }
        .bind(to: userNameRemainderLabel.rx.text)
        .disposed(by: disposeBag)
        
        userNameValidObservable
        .bind(to: userNameValidationLabel.rx.isHidden)
        .disposed(by: disposeBag)
        
        passwordValidObservable
        .bind(to: passwordInvalidLabel.rx.isHidden)
        .disposed(by: disposeBag)
    }
}
